(function($) {
  $('form').each(function(){
    $(this).validate({
      unhighlight: function (element, errorClass) {
        $(element).removeClass('input_error');
      },
      submitHandler: function(form, e) {
        e.preventDefault();
        var form = $(form),
            str = form.serialize();
        $.ajax({
          url: '#',
          type: 'get',
          data: str
        })
        .done(function() {
           yaCounter###.reachGoal(goal);
           $('.modal').closeModal();
           $('#ok').openModal();
        })
        .always(function() {
        });
      },
      rules: {
        'phone': {
          required: true,
        },
        'name': {
          required: true
        },
        'email': {
          required: true,
          email: true
        }
      },
      errorPlacement: function(error, element){
        $(element).addClass('input_error');
      }
    });
  });
})(jQuery);