$(document).ready(function(){
  $('body, .modal').niceScroll({
    cursorborder: "0", 
    cursorcolor:"#9fbef7",
    scrollspeed: 100,
    mousescrollstep: 25,
    cursorwidth: "6px",
    cursorborderradius: "5px",
    oneaxismousemode: false,
    cursordragontouch: true,
    rtlmode: false,
    horizrailenabled: false,
    zindex: 1000
  });

  $('.modalopen').click(function(){
    $('.modal').openModal();
  });
});