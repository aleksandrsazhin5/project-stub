var gulp = require('gulp'),
  livereload = require('gulp-livereload'),
  server = require('gulp-server-livereload'),
  gutil = require('gulp-util'),
  
  clean = require('gulp-clean'),
  rename = require('gulp-rename'),
  
  jade = require('gulp-jade'),
  stylus = require('gulp-stylus'),
  autoprefixer = require('gulp-autoprefixer'),
  
  usemin = require('gulp-usemin'),
  
  minifyCss = require('gulp-minify-css'),
  uglify = require('gulp-uglify'),
  minifyHtml = require('gulp-minify-html'),
  
  sftp = require('gulp-sftp');
  
handleError = function(err){
  console.log('--------------------------------');
  console.log('             error              ');
  console.log('--------------------------------');
  gutil.log(err);
  console.log('--------------------------------');
  gutil.beep();
}

//Clean
gulp.task('clean', function () {
  return gulp.src(['build/**', 'css/**', '*.html'], {read: false})
    .pipe(clean());
});

//Server
gulp.task('server', function() {
  return gulp.src('./')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

//Stylus
gulp.task('stylus', function () {
  return gulp.src('stylus/*.styl')
    .pipe(stylus())
    .on('error', handleError)
    .pipe(autoprefixer({
      browsers: ['last 15 versions']
    }))
    //.pipe(gulp.dest('css'))
    //.pipe(rename({
    //  suffix: "-min",
    //  extname: ".css"
    //}))
    .pipe(gulp.dest('css'));
});

//Jade
gulp.task('jade', function(){
  return gulp.src('jade/pages/*.jade')
    .pipe(jade({
      pretty: true
    }))
    .on('error', handleError)
    .pipe(gulp.dest(''));
});

//JavaScript
gulp.task('scripts', function () {
  return gulp.src(['scripts/**/*.js', '!scripts/**/*-min.js'])
  //.pipe(rename({
  //  suffix: "-min",
  //  extname: ".js"
  //}))
  //.pipe(gulp.dest('scripts'));
});

//Images
gulp.task('images', function() {
  return gulp.src('images/*.{jpg,jpeg,png,gif}')
    .pipe(gulp.dest("build/images"));
});

//Fonts
gulp.task('fonts', function() {
  return gulp.src('fonts/**/*.{otf,ttf,woff,woff2,eot,svg}')
    .pipe(gulp.dest("build/fonts"));
});

//.htaccess
gulp.task('htaccess', function() {
  return gulp.src('.htaccess')
    .pipe(gulp.dest("build"));
});

//Watch
gulp.task('watch', function(){
  gulp.watch('jade/**/*.jade', ['jade', 'livereload']);
  gulp.watch(['scripts/**/*.js', '!scripts/**/*-min.js'], ['livereload']);
  gulp.watch('fonts/**/*.{otf,ttf,woff,woff2,eot,svg}', ['livereload']);
  gulp.watch('images/**/*.{jpg,jpeg,png,gif}', ['livereload']);
  gulp.watch('stylus/**/*.styl', ['stylus', 'livereload']);
  gulp.start('server');
});

//livereload
gulp.task('livereload', function () {
	livereload();
});

//Run
gulp.task('run', [
	'stylus', 
	'jade'
]);

//Default
gulp.task('default', ['run'], function(){
  gulp.start('watch');
});

//Build
gulp.task('build',
	[
	  'clean',
	  'images', 
	  'fonts',  
	  'htaccess',
	  'run'
	], 
	function(){
	  return gulp.src('*.html')
		.pipe(usemin({
		  css: [minifyCss({compatibility: 'ie8'}), 'concat'],
		  html: [minifyHtml({empty: true})],
		  js: [uglify()]
		}))
		.pipe(gulp.dest('build'));
	}
);

//FTP
gulp.task('sftp', function () {
  gulp.src('build/**/*')
    .pipe(sftp({
      host: 'website.com',
      user: 'johndoe',
      pass: '1234',
      remotePath: '/'
    }));
});